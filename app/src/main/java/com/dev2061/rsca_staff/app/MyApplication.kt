package com.dev2061.rsca_staff.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication() : Application() {
}