package com.dev2061.rsca_staff.di

import android.app.Application
import androidx.room.Room
import com.dev2061.rsca_staff.model.api.PlayersService
import com.dev2061.rsca_staff.util.Constants
import com.dev2061.rscaplayers.app.model.room.PlayersDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun providesBaseUrl() = Constants.BASE_URL

    @Provides
    @Singleton
    fun providesRetrofitInstance(BASE_URL: String): PlayersService =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(PlayersService::class.java)

    @Provides
    @Singleton
    fun providesDatabase(app: Application) =
        Room.databaseBuilder(app, PlayersDatabase::class.java, "players_database")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    fun providesPlayersDao(db: PlayersDatabase) = db.playersDao()
}