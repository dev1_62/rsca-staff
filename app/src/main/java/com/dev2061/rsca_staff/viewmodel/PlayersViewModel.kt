package com.dev2061.rsca_staff.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.dev2061.rsca_staff.model.repository.PlayersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlayersViewModel @Inject constructor(repository: PlayersRepository) :
    ViewModel() {

    var players = repository.getPlayers().asLiveData()

}