package com.dev2061.rsca_staff.ui.list.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.dev2061.rsca_staff.databinding.RowListCategoryBinding
import com.dev2061.rsca_staff.databinding.RowListPlayersBinding
import com.dev2061.rsca_staff.model.data.Data


sealed class PlayersListViewHolder(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {

    var itemClickListener: ((view: View, item: Data, position: Int) -> Unit)? = null

    class HeaderHolder(val binding: RowListCategoryBinding) : PlayersListViewHolder(binding) {
        fun bind(row: AdapterRow.TitleItem) {
            binding.tvRoles.text = row.category
        }
    }

    class DetailsHolder(val binding: RowListPlayersBinding) : PlayersListViewHolder(binding) {
        fun bind(row: AdapterRow.ListItem) {
            Glide.with(binding.root)
                .load(row.player.avatar)
                .into(binding.playersImage)

            binding.playersName.text = "${row.player.first_name} ${row.player.last_name}"

            val number = row.player.shirt_number ?: -1

            if (number != -1) {
                binding.tvCounter.text = row.player.shirt_number?.toString()
            } else {
                binding.tvCounter.visibility = View.GONE
            }

            binding.root.setOnClickListener {
                itemClickListener?.invoke(it, row.player, adapterPosition)
            }
        }
    }
}

