package com.dev2061.rsca_staff.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev2061.rsca_staff.databinding.FragmentListPlayersBinding
import com.dev2061.rsca_staff.ui.list.adapter.PlayersListAdapter
import com.dev2061.rsca_staff.util.Resource
import com.dev2061.rsca_staff.util.mapData
import com.dev2061.rsca_staff.viewmodel.PlayersViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListPlayersFragment : Fragment() {

    private var _binding: FragmentListPlayersBinding? = null
    private val binding get() = _binding!!

    val vm: PlayersViewModel by viewModels()
    private lateinit var myAdapter: PlayersListAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListPlayersBinding.inflate(inflater, container, false)
        val view = binding.root

        setupRv()
        observeVm()
        return view
    }


    private fun setupRv() {

        myAdapter = PlayersListAdapter()
        binding.parentRecyclerview.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(
                activity, LinearLayoutManager.VERTICAL, false
            )
        }

        myAdapter.itemClickListener = { view, player, position ->

            val action =
                ListPlayersFragmentDirections.actionListPlayersFragmentToDetailPlayerFragment(player)
            view.findNavController().navigate(action)
        }
    }

    private fun observeVm() {
        vm.players.observe(viewLifecycleOwner) { result ->

            result.let {
                myAdapter.differ.submitList(mapData(it))
            }

            binding.pbLoading.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
            binding.tvError.apply {
                isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                text = result.error?.localizedMessage
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}