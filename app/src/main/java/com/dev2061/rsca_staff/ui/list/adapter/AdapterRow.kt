package com.dev2061.rsca_staff.ui.list.adapter

import com.dev2061.rsca_staff.model.data.Data


sealed class AdapterRow {
    data class TitleItem(val category: String) : AdapterRow()
    data class ListItem(val player: Data) : AdapterRow()
}

