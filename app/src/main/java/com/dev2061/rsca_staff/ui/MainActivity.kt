package com.dev2061.rsca_staff.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dev2061.rsca_staff.R
import com.dev2061.rsca_staff.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
}