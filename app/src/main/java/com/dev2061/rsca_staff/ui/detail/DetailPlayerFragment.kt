package com.dev2061.rsca_staff.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.dev2061.rsca_staff.databinding.FragmentDetailPlayerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailPlayerFragment : Fragment() {

    private var _binding: FragmentDetailPlayerBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<DetailPlayerFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailPlayerBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val player = args.player

        binding.apply {
            Glide.with(view)
                .load(player?.image)
                .fitCenter()
                .into(ivPicture)

            val number = player?.shirt_number ?: -1

            if (number != -1) {
                tvNumber.text = player?.shirt_number?.toString()
            } else {
                tvNumber.visibility = View.GONE
            }

            tvName.text = "${player?.first_name?.uppercase()} ${player?.last_name?.uppercase()}"

            tvAtTheClub.text = player?.start_date
            tvBirthday.text = player?.birth_date
            tvCountry.text = player?.country
            //tvHeight.text = player?.
            tvPosition.text = player?.category
            tvShirtName.text = player?.shirt_name

            tvEscape.setOnClickListener {
                val action =
                    DetailPlayerFragmentDirections.actionDetailPlayerFragmentToListPlayersFragment()
                view.findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}