package com.dev2061.rsca_staff.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev2061.rsca_staff.R
import com.dev2061.rsca_staff.databinding.RowListCategoryBinding
import com.dev2061.rsca_staff.databinding.RowListPlayersBinding
import com.dev2061.rsca_staff.model.data.Data


class PlayersListAdapter() : RecyclerView.Adapter<PlayersListViewHolder>() {

    val differ = AsyncListDiffer(this, DiffCallback())

    var itemClickListener : ((view: View, player: Data, position : Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersListViewHolder {
        return when (viewType) {
            R.layout.row_list_category -> PlayersListViewHolder.HeaderHolder(
                RowListCategoryBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.row_list_players -> PlayersListViewHolder.DetailsHolder(
                RowListPlayersBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid ViewType Provided")
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (differ.currentList[position]) {
            is AdapterRow.TitleItem -> R.layout.row_list_category
            is AdapterRow.ListItem -> R.layout.row_list_players
        }
    }


    override fun onBindViewHolder(holder: PlayersListViewHolder, position: Int) {
        holder.itemClickListener = itemClickListener
        when (holder) {
            is PlayersListViewHolder.HeaderHolder -> holder.bind(differ.currentList[position] as AdapterRow.TitleItem)
            is PlayersListViewHolder.DetailsHolder -> holder.bind(differ.currentList[position] as AdapterRow.ListItem)
        }
    }

    override fun getItemCount() = differ.currentList.size


    private class DiffCallback : DiffUtil.ItemCallback<AdapterRow>() {
        override fun areItemsTheSame(oldItem: AdapterRow, newItem: AdapterRow): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: AdapterRow, newItem: AdapterRow): Boolean {
            return oldItem == newItem
        }
    }
}