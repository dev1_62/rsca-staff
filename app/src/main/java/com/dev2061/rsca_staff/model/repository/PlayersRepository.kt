package com.dev2061.rsca_staff.model.repository

import androidx.room.withTransaction
import com.dev2061.rsca_staff.model.api.PlayersService
import com.dev2061.rsca_staff.util.networkBoundResource
import com.dev2061.rscaplayers.app.model.room.PlayersDatabase
import kotlinx.coroutines.delay
import javax.inject.Inject

class PlayersRepository @Inject constructor(
    private val apiService: PlayersService,
    private val db: PlayersDatabase
) {

    private val playersDao = db.playersDao()

    fun getPlayers() = networkBoundResource(
        query = {
            playersDao.getAllPlayers()
        },
        fetch = {
            delay(2000)
            apiService.getPlayers()
        },
        saveFetchResult = { players ->
            db.withTransaction {
                playersDao.deleteAll()
                players.body()?.data?.let { playersDao.insertPlayers(it) }
            }
        }
    )
}