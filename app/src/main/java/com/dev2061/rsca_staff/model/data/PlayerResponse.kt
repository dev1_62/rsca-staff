package com.dev2061.rsca_staff.model.data


data class PlayerResponse(
    val _links: Links,
    val _meta: Meta,
    val `data`: MutableList<Data>
)