package com.dev2061.rsca_staff.model.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "players_table")
data class Data(
    val active: Boolean?,
    val avatar: String?,
    val birth_date: String?,
    val category: String?,
    val country: String?,
    val first_name: String?,
    @PrimaryKey val id: Int?,
    val image: String?,
    val last_name: String?,
    val nationality: String?,
    val shirt_name: String?,
    val shirt_number: Int?,
    val start_date: String?
) : Parcelable


