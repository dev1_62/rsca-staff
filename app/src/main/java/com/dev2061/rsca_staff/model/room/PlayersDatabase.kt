package com.dev2061.rscaplayers.app.model.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dev2061.rsca_staff.model.data.Data

@Database(entities = [Data::class], version = 1)
abstract class PlayersDatabase : RoomDatabase() {

    abstract fun playersDao(): PlayersDao
}