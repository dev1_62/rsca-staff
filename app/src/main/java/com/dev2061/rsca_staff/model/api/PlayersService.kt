package com.dev2061.rsca_staff.model.api

import com.dev2061.rsca_staff.model.data.PlayerResponse
import com.dev2061.rsca_staff.util.Constants
import retrofit2.Response
import retrofit2.http.GET

interface PlayersService {

    @GET(Constants.PLAYERS_ENDPOINT)
    suspend fun getPlayers(): Response<PlayerResponse>

}