package com.dev2061.rscaplayers.app.model.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dev2061.rsca_staff.model.data.Data
import kotlinx.coroutines.flow.Flow

@Dao
interface PlayersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlayers(players: List<Data>)

    @Query("SELECT * FROM players_table")
    fun getAllPlayers(): Flow<List<Data>>

    @Query("DELETE FROM players_table")
    suspend fun deleteAll()

}