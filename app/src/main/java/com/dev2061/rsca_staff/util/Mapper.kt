package com.dev2061.rsca_staff.util

import com.dev2061.rsca_staff.model.data.Data
import com.dev2061.rsca_staff.ui.list.adapter.AdapterRow


fun mapData(rawList: Resource<List<Data>>): MutableList<AdapterRow> {
    rawList.let {
        var myList = mutableListOf<AdapterRow>()
        val groupedByCategory = it.data?.groupBy { it.category }
        myList = ArrayList<AdapterRow>()

        for (i in groupedByCategory?.keys!!) {
            myList.add(AdapterRow.TitleItem(i!!))
            for (v in groupedByCategory.getValue(i)) {
                myList.add(AdapterRow.ListItem(v))
            }
        }
        return myList
    }
}