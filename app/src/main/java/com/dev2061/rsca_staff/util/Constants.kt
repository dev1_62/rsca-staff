package com.dev2061.rsca_staff.util


class Constants {

    companion object {
        const val BASE_URL = "https://rsca-0002-prod.novemberfive.co/"
        const val PLAYERS_ENDPOINT = "api/employees"
    }
}
